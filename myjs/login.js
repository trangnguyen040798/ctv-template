function toggleEye() {
    if($('.icon-password i').hasClass('fa-eye')) {
        $('.icon-password i').removeClass('fa-eye');
        $('.icon-password i').addClass('fa-eye-slash');
        $('input[name=password]').attr('type', 'text');
    } else {
        $('.icon-password i').addClass('fa-eye');
        $('.icon-password i').removeClass('fa-eye-slash');
        $('input[name=password]').attr('type', 'password');
    }
}
